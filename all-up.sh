kubectl create -f deployments/flask.yaml
kubectl create -f services/flask.yaml
kubectl create -f deployments/auth.yaml
kubectl create -f services/auth.yaml
kubectl create configmap nginx-frontend-conf --from-file=nginx/frontend.conf
kubectl create configmap nginx-proxy-conf --from-file=nginx/proxy.conf
kubectl create secret generic tls-certs --from-file tls/
kubectl create -f deployments/frontend.yaml
kubectl create -f services/frontend.yaml
