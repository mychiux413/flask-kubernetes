Quickstart
=======
    # create 5 nodes
    gcloud container clusters create bootcamp --num-nodes 5 --scopes "https://www.googleapis.com/auth/projecthosting,storage-rw";
    
    bash all-up.sh

    # Check pods status
    watch -n 1 kubectl get pods

    # find External IP and open with browser: https://<EXTERNAL-IP>/numpy
    kubectl get services